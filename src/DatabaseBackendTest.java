import java.sql.SQLException;


public class DatabaseBackendTest {

	/**
	 * main method, class to test the DatabaseBackend
	 */
	public static void main(String[] args) {
		DatabaseBackend db = new DatabaseBackend();
		String[] output = new String[15];
		try {
			boolean hasUser1 = db.User("asd");
			boolean hasUser2 = db.User("bob");
			boolean hasUser3 = db.User("alex");
			boolean correctPass = db.Pass("hello123");
			output[0] = db.Stat();
			output[1] = db.List(-1);
			output[2] = db.List(1);
			output[3] = db.List(4);
			output[4] = db.Dele(4);
			output[5] = db.Dele(1);
			output[6] = db.Dele(1);
			output[7] = db.Uidl(-1);
			output[8] = db.Uidl(4);
			output[9] = db.Uidl(1);
			output[10] = db.Rset();
			output[11] = db.Stat();
			output[12] = db.RetrOrTop(1, 3);
			output[13] = db.RetrOrTop(2, 3);
			output[14] = db.Quit();
			
			System.out.println(hasUser1);
			System.out.println(hasUser2);
			System.out.println(hasUser3);
			System.out.println(correctPass);
			for(int i = 0; i < 15; i++){
				System.out.print(output[i]);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		

	}

}
