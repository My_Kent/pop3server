import java.sql.*;
import java.util.ArrayList;


/**
 * Implement the method in the databaseInterface to handle the command in command interpreter,
 */
public class DatabaseBackend implements DatabaseInterface {
	private Statement stmt;
	private int iMaildropID;
	private Connection connection;
	private int amount;

	/**
	 * Default constructor, once an object of DatabaseBackend is created, set
	 * the JDBC to connect to the database zxc03u with username and password.
	 * Catch exception if fails
	 */
	DatabaseBackend() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:8889/zxc03u", "root",
					"root");
			stmt = connection.createStatement();
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}

	/**
	 * Lock() method, once username and password are entered correctly, lock the
	 * maildrop to prevent more than two modification to one maildrop at one
	 * time
	 */
	public void Lock() throws SQLException {
		stmt.executeUpdate("update m_Maildrop set tiLocked = 1 where iMaildropID = '"
				+ iMaildropID + "'");
	}

	/**
	 * isLocked method, when user try to log in, after entered correct username
	 * and password, check whether the maildrop is locked or not
	 */
	public boolean isLocked() throws SQLException {
		ResultSet res = stmt
				.executeQuery("Select tiLocked from m_Maildrop where iMaildropID = '"
						+ iMaildropID + "'");
		res.next();
		if (res.getInt(1) == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * clear() method, set the attribute markedForDeletion in table m_Mail to
	 * the default value 0, which means all the message in the maildrop is not
	 * marked as deleted
	 */
	private void clear() throws SQLException {
		stmt.executeUpdate("update m_Mail set markedForDeletion = 0 where iMaildropID = '"
				+ iMaildropID + "'");
	}

	/**
	 * One type of method, used for closing the connection accidently, for
	 * example timeout or exception. This method is used to unlocked the current
	 * maildrop for the next time using and rset the maildrop.
	 */
	public void TimeoutQuit() throws SQLException {
		stmt.executeUpdate("update m_Maildrop set tiLocked = 0 where iMaildropID = '"
				+ iMaildropID + "'");
		clear();
		//close the connection of database after timeout quit
		stmt.close();
		connection.close();
	}

	/**
	 * Update method, The method dele the messages in the maildrop which have
	 * been marked.
	 */
	public void Update() throws SQLException {
		stmt.executeUpdate("delete from m_Mail where markedForDeletion = '1'");
		// close the connection of database after timeout quit
		stmt.close();
		connection.close();
	}

	/**
	 * User method, check whether the username entered by the user exists. If
	 * exists, set the iMaildropID for the afterward using and return true,
	 * else, return false.
	 */
	public boolean User(String s1) throws SQLException {
		ResultSet res = stmt
				.executeQuery("Select iMaildropID from m_Maildrop where vchUsername = '"
						+ s1 + "'");
		if (res.next()) {
			iMaildropID = res.getInt(1);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check whether the password is correct related to username entered by user
	 * earlier. Return true if the password is correct, otherwise, false.
	 */
	public boolean Pass(String s1) throws SQLException {
		ResultSet res = stmt
				.executeQuery("Select vchPassword from m_Maildrop where iMaildropID = '"
						+ iMaildropID + "'");
		res.next();
		String password = res.getString(1);
		if (password.equals(s1)) {
			ResultSet res1 = stmt
					.executeQuery("select * from m_Mail where iMaildropID = '"
							+ iMaildropID + "'");
			// set the amount of the message in the maildrop for afterward
			// using, such as check whether the message exists
			while (res1.next()) {
				amount++;
			}
			return true;

		} else {
			return false;
		}
	}

	/**
	 * Quit method, changes the maildrop unlocked and delete the messages have
	 * been marked. If the password is not correct entered, does not matter, no
	 * changes will be happened to the maildrop while execute the following code
	 */
	public String Quit() throws SQLException {
		stmt.executeUpdate("update m_Maildrop set tiLocked = 0 where iMaildropID = '"
				+ iMaildropID + "'");
		Update();
		return "+OK dewey POP3 server signing off \r\n";
	}

	/**
	 * Return current number and size of the maildrop (not including the
	 * messages marked deleted)
	 */
	public String Stat() throws SQLException {
		ResultSet res = stmt
				.executeQuery("select iMailID, txMailContent, markedForDeletion from m_Mail where iMaildropID = '"
						+ iMaildropID + "'");
		int numberOfletter = 0;
		int size = 0;
		int index = 1;
		while (res.next()) {
			if (res.getInt(3) == 0) {
				numberOfletter++;
				size += res.getString(2).length();
			}
			index++;
		}
		return "+OK " + numberOfletter + " " + size + "\r\n";
	}

	/**
	 * List method. Return all the size of each message if without argument.
	 * Otherwise, return the size of the message that user want; Error if the
	 * message does not exists
	 */
	public String List(int i) throws SQLException {
		String output = "";
		// without argument. Because commandInterpreter has selected the valid
		// arguments, therefore it is fine to send i = -1
		if (i == -1) {
			int index = 1;
			int number = 0;
			int size = 0;
			ResultSet res = stmt
					.executeQuery("select iMailID, txMailContent, markedForDeletion from m_Mail where iMaildropID = '"
							+ iMaildropID + "'");
			while (res.next()) {
				// check whether message is marked deleted or not
				if (res.getInt(3) == 0) {
					output += index + " " + res.getString(2).length() + "\r\n";
					// sum of the number and size of the message has been read
					// currently
					number++;
					size += res.getString(2).length();
				}
				index++;
			}
			String temp = "+OK there are " + number + " messages  (" + size
					+ " octets) \r\n";
			output = temp + output;
			output += ".\r\n";
			// list with argument.
		} else {
			if (i > amount) {
				output += "-ERR no such message, there are only " + amount
						+ " in the maildrop \r\n";
			} else {
				int count = 1;
				ResultSet res = stmt
						.executeQuery("select iMailID, txMailContent, markedForDeletion from m_Mail where iMaildropID = '"
								+ iMaildropID + "'");
				while (res.next()) {
					// get the message the user want
					if (count == i) {
						// check whether the message is marked deleted
						if (res.getInt(3) == 0) {
							output += "+OK " + count + " "
									+ res.getString(2).length() + "\r\n";
						} else {
							output += "-ERR message " + i
									+ " has been deleted \r\n";
						}
						break;
					}
					count++;
				}
			}
		}
		return output;
	}

	/**
	 * Delete method, marked the message as deleted if the message exists and
	 * has not been marked, otherwise return -ERR
	 */
	public String Dele(int i) throws SQLException {
		String output = "";
		if (i > amount) {
			output += "-ERR no such message, there are only " + amount
					+ " in the maildrop \r\n";
		} else {
			ResultSet res = stmt
					.executeQuery("select iMailID, markedForDeletion from m_Mail where iMaildropID = '"
							+ iMaildropID + "'");
			int count = 1;
			while (res.next()) {
				// get the message user want
				if (count == i) {
					// mark the message as deleted if exists and has not been
					// marked
					if (res.getInt(2) == 0) {
						stmt.executeUpdate("update m_Mail set markedForDeletion = 1 where iMailID = '"
								+ res.getInt(1) + "'");
						output += "+OK message " + i + " deleted \r\n";
					} else {
						return "-ERR message" + i + " already deleted \r\n";
					}
					break;
				}
				count++;
			}
		}
		return output;
	}

	/**
	 * Change all the messages to not marked and return the number and size of
	 * the maildrop
	 */
	public String Rset() throws SQLException {
		clear();
		ResultSet res1 = stmt
				.executeQuery("select txMailContent from m_Mail where iMaildropID = '"
						+ iMaildropID + "'");
		int number = 0;
		int size = 0;
		while (res1.next()) {
			// calculate the number and size of the whole messages in the
			// maildrop
			number++;
			size += res1.getString(1).length();
		}
		return "+OK maildrop has " + number + " messages (" + size
				+ " octets) \r\n";
	}

	/**
	 * Retr or Top method. while j == -1, return all the content of the message
	 * if exists and has not been deleted. else, return the top j line of
	 * content of the message with the header if exists and has not been deleted
	 */
	public String RetrOrTop(int i, int j) throws SQLException {
		String output = "";
		if (i > amount) {
			output = "-ERR no such message, there are only " + amount
					+ " in the maildrop \r\n";
		} else {
			ResultSet res = stmt
					.executeQuery("select txMailContent, markedForDeletion from m_Mail where iMaildropID = '"
							+ iMaildropID + "'");
			int count = 1;
			output += "+OK \r\n";
			while (res.next()) {

				if (count == i) {
					if (res.getInt(2) == 0) {
						// RETR method
						if ((j == -1)) {
							String[] temp = res.getString(1).split("\n\n");
							String header = "<" + temp[0] + "> \n\n";
							String content = "";
							for (int n = 1; n < temp.length - 1; n++) {
								content += temp[n] + "\n\n";
							}
							content += temp[temp.length - 1];
							output += header + content + "\r\n.\r\n";
						} else {
							// there are a blank line between header and
							// content, therefore split "\n\n"
							String[] temp = res.getString(1).split("\n\n");
							String[] eachLine;
							String body ="";
							for (int a = 1; a < temp.length; a++) {
								// String list, get each line of the content as
								// one argument
								eachLine = temp[a].split("\n");
								for (int b = 0; b < eachLine.length; b++) {
									if (j == 0) {
										break;
									}
									body += eachLine[b] + "\r\n";
									j--;
								}
								if (j == 0) {
									break;
								}
								body += "\r\n";
								j--;
							}
							// return <header> the top j line of message i
							output += "<" + temp[0] + ">" + "\r\n\r\n" + body
									+ ".\r\n";
						}
					} else {
						output = "-ERR message" + i + " has been deleted \n";
					}
					break;
				}
				count++;
			}
		}
		return output;
	}

	/**
	 * return the index and uidl of all the messages if without arguments,
	 * return the index and uidl if with arguments and the message exists and
	 * has not been deleted
	 */
	public String Uidl(int i) throws SQLException {
		String output = "";
		if (i > amount) {
			output += "-ERR no such message, there are only " + amount
					+ " in the maildrop \r\n";
		} else {
			int count = 1;

			ResultSet res = stmt
					.executeQuery("select iMailID, vchUIDL, markedForDeletion from m_Mail where iMaildropID = '"
							+ iMaildropID + "'");
			// without arguments, because the arguments has been selected, which
			// are all positive, therefore -1 can be used standing for no
			// arguments
			if (i == -1) {
				output += "+OK \r\n";
				while (res.next()) {
					// check whether the message has been deleted
					if (res.getInt(3) == 0) {
						output += count + " " + res.getString(2) + "\r\n";
					}
					count++;
				}
				output += ".\r\n";
			} else {
				// with arguments
				while (res.next()) {
					if (count == i) {
						// check whether message has been deleteds
						if (res.getInt(3) == 0) {
							output += "+OK " + count + " " + res.getString(2)
									+ "\r\n";
						} else {
							output += "-ERR message " + i
									+ " has been deleted \r\n";
						}
						break;
					}
					count++;
				}
			}

		}
		return output;
	}

}