import java.sql.SQLException;

/**
 * Changes in the database interface. Add the method that link the
 * commandInterpreter and DatabaseBackend
 */
public interface DatabaseInterface {
	public abstract boolean isLocked() throws SQLException;

	public abstract void Lock() throws SQLException;

	public abstract void TimeoutQuit() throws SQLException;

	public abstract void Update() throws SQLException;

	public abstract boolean User(String s1) throws SQLException;

	public boolean Pass(String s1) throws SQLException;

	public abstract String Quit() throws SQLException;

	public abstract String Stat() throws SQLException;

	public abstract String List(int i) throws SQLException;

	public abstract String Dele(int i) throws SQLException;

	public abstract String Rset() throws SQLException;

	public abstract String RetrOrTop(int i, int j) throws SQLException;

	public abstract String Uidl(int i) throws SQLException;

}
