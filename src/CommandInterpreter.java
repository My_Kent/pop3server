import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class CommandInterpreter {
	private DatabaseInterface db = new DatabaseBackend();
	private String[] commandInput;
	private boolean isLogedInUSER = false;
	private boolean isLogedInPASS = false;
	private final int Authorization = 1;
	private final int Transaction = 2;
	private int state = 1;

	// default construct
	CommandInterpreter() {
	}

	/**
	 * Method that handle receives messages from the network part of Part 2 to
	 * recognized which command it is.
	 */
	protected String handleInput(String input) throws IndexOutOfBoundsException {
		commandInput = input.split(" ");
		commandInput[0] = commandInput[0].toUpperCase();
		switch (state) {
		// State Authorization
		case Authorization:
			switch (commandInput[0]) {
			case "USER":

				int[] argumentNumber1 = { 1 };
				boolean amountMatch1 = handleArgument(argumentNumber1);
				if (amountMatch1) {
					return User();
				} else {
					return "-ERR The number of arguments is not correct, try again \r\n";
				}

			case "PASS":
				if (!isLogedInUSER) {
					return "-ERR Enter you username first\r\n";
				} else {
					int[] argumentNumber2 = { 1 };
					boolean amountMatch2 = handleArgument(argumentNumber2);
					if (amountMatch2) {
						return Pass();
					} else {
						return "-ERR The number of arguments is not correct, try again \r\n";
					}
				}
			case "QUIT":
				int[] argumentNumber3 = { 0 };
				boolean amountMatch3 = handleArgument(argumentNumber3);
				if (amountMatch3) {
					return Quit();
				} else {
					return "-ERR The number of arguments is not correct, try again \r\n";
				}
			default:
				return "-ERR Command is not available in Authorization state \r\n";
			}
			// Transaction state
		case Transaction:
			switch (commandInput[0]) {
			case "QUIT":
				int[] argumentNumber3 = { 0 };
				boolean amountMatch3 = handleArgument(argumentNumber3);
				if (amountMatch3) {
					return Quit();
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}

			case "STAT":
				int[] argumentNumber4 = { 0 };
				boolean amountMatch4 = handleArgument(argumentNumber4);
				if (amountMatch4) {
					return Stat();
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}

			case "LIST":
				int[] argumentNumber5 = { 0, 1 };
				boolean amountMatch5 = handleArgument(argumentNumber5);
				boolean typeMatch5 = handleArgument(commandInput[0]);
				if (amountMatch5 && typeMatch5) {
					return List();
				} else if (!amountMatch5) {
					return "-ERR The number of arguments is not correct, try again \r\n";
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}

			case "RETR":
				int[] argumentNumber6 = { 1 };
				boolean amountMatch6 = handleArgument(argumentNumber6);
				boolean typeMatch6 = handleArgument(commandInput[0]);
				if (amountMatch6 && typeMatch6) {
					return RetrOrTop();
				} else if (!amountMatch6) {
					return "-ERR The number of arguments is not correct, try again \r\n";
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}
			case "DELE":
				int[] argumentNumber7 = { 1 };
				boolean amountMatch7 = handleArgument(argumentNumber7);
				boolean typeMatch7 = handleArgument(commandInput[0]);
				if (amountMatch7 && typeMatch7) {
					return Dele();
				} else if (!amountMatch7) {
					return "-ERR The number of arguments is not correct, try again \n";
				} else {
					return "-ERR The argument should be positive integer type \n";
				}
			case "NOOP":
				int[] argumentNumber8 = { 0 };
				boolean amountMatch8 = handleArgument(argumentNumber8);
				if (amountMatch8) {
					return Noop();
				} else {
					return "-ERR The number of arguments is not correct, try again \r\n";
				}
			case "RSET":
				int[] argumentNumber9 = { 0 };
				boolean amountMatch9 = handleArgument(argumentNumber9);
				if (amountMatch9) {
					return Reset();
				} else {
					return "-ERR TThe number of arguments is not correct, try again \r\n";
				}
			case "TOP":
				int[] argumentNumber10 = { 2 };
				boolean amountMatch10 = handleArgument(argumentNumber10);
				boolean typeMatch10 = handleArgument(commandInput[0]);
				if (amountMatch10 && typeMatch10) {
					return RetrOrTop();
				} else if (!amountMatch10) {
					return "-ERR The number of arguments is not correct, try again \r\n";
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}
			case "UIDL":
				int[] argumentNumber11 = { 0, 1 };
				boolean amountMatch11 = handleArgument(argumentNumber11);
				boolean typeMatch11 = handleArgument(commandInput[0]);
				if (amountMatch11 && typeMatch11) {
					return Uidl();
				} else if (!amountMatch11) {
					return "-ERR The number of arguments is not correct, try again \r\n";
				} else {
					return "-ERR The argument should be positive integer type \r\n";
				}
			default:
				return "-ERR Command is not available in Transaction state \r\n";
			}
		default:
			return "-ERR";
		}
	}

	/**
	 * Method handleArgument return true if the amount of arguments matches
	 * requirements return false otherwise
	 */
	private boolean handleArgument(int[] argumentNumber) {
		for (int i = 0; i <= argumentNumber.length - 1; i++) {
			if (commandInput.length - 1 == argumentNumber[i]) {
				return true;
			}
		}
		return false;
	}

	protected void TimeoutQuit() {
		try {
			db.TimeoutQuit();
		} catch (SQLException e) {
		}
	}

	/**
	 * Method handleArgument return true if the arguments can be convent into
	 * integer type else return false
	 */
	private boolean handleArgument(String inputString) {
		int length = commandInput.length;
		try {
			for (int i = 1; i <= length - 1; i++) {
				int temp = Integer.parseInt(commandInput[i]);
				if (temp <= 0) {
					return false;
				}
			}
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	/**
	 * Method USER, Be able to enter the PASS if Username is exists, otherwise,
	 * log in again or quit.
	 */
	private String User() {
		// check whether the user name exists
		try {
			boolean hasUser = db.User(commandInput[1]);
			if (hasUser) {
				isLogedInUSER = true;
				// copy the userName
				return "+OK name is a valid mailbox \r\n";
			} else {
				return "-ERR never heard of mailbox name \r\n";
			}
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * Method PASS, be able to log into the mailbox if PASS is correct with the
	 * relevant userName otherwise, log in again or quit.
	 */
	private String Pass() {
		try {
			if (isLogedInUSER) {
				if (db.Pass(commandInput[1])) {
					if (db.isLocked()) {
						return "-ERR The maildrop has been locked \r\n";
					} else {
						state = 2;
						db.Lock();
						//return "+OK \r\n";
						 return db.Stat();
					}
				} else {
					return "-ERR invalid password \r\n";
				}

			} else {
				return "-ERR enter user name first \r\n";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "-ERR Exception";
		}
	}

	/**
	 * Method Quit quit the mailbox system, if user want to check any message of
	 * the mailbox, he/ she need to log in again
	 */
	private String Quit() {
		try {
			String output = db.Quit();
			state = 1;
			isLogedInUSER = false;
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * Method Stat return the the number of message and the size of the mailbox
	 * in octets
	 */
	private String Stat() {
		try {
			String output = db.Stat();
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * Method List If an argument was given, scan listing for that message
	 * otherwise, scan each message in the maildrop.
	 */
	private String List() {
		try {
			if (commandInput.length == 1) {
				String output = db.List(-1);
				return output;
			} else {
				String output = db.List(Integer.parseInt(commandInput[1]));
				return output;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "-ERR Exception";
		}
	}

	/**
	 * Method Dele Delete the message corresponding to the given number if
	 * exists, but the server does not actually delete the message until the
	 * POP3 session enters the UPDATE state otherwise, return "error" string
	 */
	private String Dele() {
		try {
			String output = db.Dele(Integer.parseInt(commandInput[1]));
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * Noop, Check the POP3 server is connected by a user
	 */
	private String Noop() {
		return "+OK \r\n";
	}

	/**
	 * Reset the the message which were marked to delete
	 */
	public String Reset() {
		try {
			String output = db.Rset();
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * refer a message or print the top n(n is input argument) line of the
	 * message
	 */
	private String RetrOrTop() {
		try {
			String output;
			if (commandInput.length == 2) {
				output = db.RetrOrTop(Integer.parseInt(commandInput[1]), -1);
			} else {
				output = db.RetrOrTop(Integer.parseInt(commandInput[1]),
						Integer.parseInt(commandInput[2]));
			}
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}

	/**
	 * print the unique id of which message
	 */
	private String Uidl() {
		try {
			int argumentNumber = commandInput.length;
			String output;
			if (argumentNumber == 1) {
				output = db.Uidl(-1);
			} else {
				output = db.Uidl(Integer.parseInt(commandInput[1]));
			}
			return output;
		} catch (SQLException e) {
			return "-ERR Exception";
		}
	}
}
