import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.net.*;

public class Pop3Server {
	/**
	 * Changes in Pop3Server: 
	 * if the client is close by timeout or other
	 * exception, called method "TimeoutQuit()" in CommandInterpreter to unlock
	 * the maildrop and clear the messages that have been marked as deleted
	 * 
	 * 
	 * Main method get two argument of integer type from command line, if fails,
	 * throw an exception. then build a serversocket with the port number given
	 * in command line and start listen once a socket is connected and accepted.
	 * Give a thread to handle and continuing listen other connection, therefore
	 * support multiple connection
	 * 
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.err
					.println("Usage: java Pop3Server <port number> <timeOut>");
			System.exit(1);
		}
		int portNumber = 0, timeOut;

		try {
			portNumber = Integer.parseInt(args[0]);
			timeOut = Integer.parseInt(args[1]);
			boolean listening = true;
			ServerSocket serverSocket = new ServerSocket(portNumber);
			System.out.println(InetAddress.getLocalHost());

			while (listening) {
				new MultiServerThread(serverSocket.accept(), timeOut).start();
			}
		} catch (IOException e) {
			System.err.println("Could not listen on port " + portNumber);
			System.exit(-1);
		} catch (NumberFormatException e1) {
			System.out
					.println("Exception. The portnumber and timeout should be integer type");
			System.exit(-1);
		} catch (IllegalArgumentException e2) {
			System.out
					.println("Exception. the port numbe and timeout should be positive");
		}
	}
}

class MultiServerThread extends Thread {
	private Socket socket = null;
	private int timeOut;
	private Timer timer;
	private CommandInterpreter command;

	// constructor
	public MultiServerThread(Socket socket, int timeOut) {
		super("MultiServerThread");
		this.socket = socket;
		this.timeOut = timeOut;
	}

	public void run() {
		try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));) {
			String inputLine, outputLine;
			// create an object of CommandInterpreter to handle the String from
			// client
			command = new CommandInterpreter();
			outputLine = "+OK \r\n";
			out.print(outputLine);
			out.flush();
			// build a timer and given the time in millisecond
			timer = new Timer(timeOut * 1000, new TimerListener());
			timer.start();
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
				timer.restart();
				outputLine = command.handleInput(inputLine);
				out.print(outputLine);
				out.flush();
				// when the commandInterpreter finish its work, the network
				// should also be disconnected
				if ((inputLine.toUpperCase()).equals("QUIT")) {
					break;
				} else if (outputLine.toUpperCase().equals("-ERR EXCEPTION")) {
					break;
				}
			}
			// if user types quit, the command interpreter finished its word and
			// the network close;
			command.TimeoutQuit();
			socket.close();
			timer.stop();
		} catch (SocketException ex) {
			System.out.println("Socket has been closed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * TimerListener when the time runs out, close the socket the stop the
	 * timer.
	 */
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				command.TimeoutQuit();
				socket.close();
				timer.stop();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
